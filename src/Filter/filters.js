import Vue from 'vue'
Vue.filter('formatCurrency', function(value) {
  return value.toLocaleString('de-DE', { style: 'currency', currency: 'EUR' })
})
