import axios from './../axios/main'
//import iziToast from './iziToaster'
const GET = (url, params = {}, failure = null) => {
  return axios
    .get(url, {
      params
    })
    .catch(
      failure ||
        (error => {
          let {
            response: {
              data: {
                error: { message: errorMessage }
              }
            }
          } = error
          console.log(errorMessage)
          //iziToast.error({ message: errorMessage })
        })
    )
}

const POST = (url, params = {}, failure = null) => {
  return axios.post(url, params).catch(
    failure ||
      (error => {
        let {
          response: {
            data: {
              error: { message: errorMessage }
            }
          }
        } = error
        console.log(errorMessage)
        // iziToast.error({
        //   message: errorMessage
        // })
      })
  )
}

const PUT = (url, params = {}, failure = null) => {
  return axios.put(url, params).catch(
    failure ||
      (error => {
        //console.log('Request.js PUT', error.response)
        let {
          response: {
            data: {
              error: { message: errorMessage }
            }
          }
        } = error
        console.log(errorMessage)
        // iziToast.error({
        //   message: errorMessage
        // })
      })
  )
}
export { GET, POST, PUT }
