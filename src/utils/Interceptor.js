import store from '../store/index'
//import router from '../router'
import axios from '../axios/main'
import Types from '../store/types'
import LocalDatabase from './LocalDatabase'

axios.interceptors.response.use(
  response => {
    return response
  },
  async error => {
    const {
      response: { status }
    } = error

    if (status === 401) {
      await store.dispatch(Types.actions.UPDATE_AUTH_RESET)
      window.location.href = '/'
    } else {
      return Promise.reject(error)
    }
  }
)

axios.interceptors.request.use(
  async function(config) {
    const token = await LocalDatabase.get(Types.localdb.USER_TOKEN)
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error)
  }
)
