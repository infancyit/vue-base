import LocalForage from 'localforage'

const LocalDatabase = {
  set(name, token) {
    LocalForage.setItem(name, token)
  },
  get(name) {
    const data = LocalForage.getItem(name)
    return data
  },
  remove(name) {
    LocalForage.removeItem(name)
  },
  clear() {
    LocalForage.clear()
  }
}

export default LocalDatabase
