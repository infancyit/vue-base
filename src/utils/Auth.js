import { POST } from './Request'
import { LOGOUT_ROUTE, LOGIN_ROUTE } from '../const/api'
import router from './../router/index'
import store from '../store/index'
import Types from '../store/types'
import iziToast from './iziToaster'

async function logout() {
  try {
    await POST(LOGOUT_ROUTE)
    await store.dispatch(Types.actions.UPDATE_AUTH_RESET)
    window.location.href = '/'
    iziToast.success({
      message: 'Successfully Logged Out.'
    })
  } catch (error) {
    iziToast.error({
      message: 'Something went wrong. Try Again'
    })
  }
}

async function login(formData) {
  try {
    const response = await POST(LOGIN_ROUTE, formData)
    await store.dispatch(Types.actions.UPDATE_USER_TOKEN, response.data)
    router.push({
      name: 'dashboard'
    })
    iziToast.success({
      message: 'Successfully Logged In.'
    })
  } catch (error) {
    iziToast.error({
      message: 'Invalid Username/password.'
    })
  }
}

export { logout, login }
