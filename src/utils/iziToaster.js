//import Vue from 'vue'
import iziToast from 'izitoast'
import 'izitoast/dist/css/iziToast.min.css'

// Here you can include some "default" settings
iziToast.settings({
  // close: false,
  pauseOnHover: true,
  timeout: 3000,
  progressBar: true,
  layout: 2,
  position: 'bottomCenter',
  transitionIn: 'bounceInDown',
  transitionOut: 'fadeOut',
  transitionInMobile: 'fadeInUp',
  transitionOutMobile: 'fadeOutDown'
})

//use in component
// this.$iziToast.info({
//   title: 'It',
//   message: 'works! :)'
// })
//in js file
//iziToast.error({ message: 'PIN_ERROR' })
export default iziToast
