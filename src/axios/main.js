import axios from 'axios'
const env = require('./../../.env')

const main = axios.create({
  baseURL: env.baseurl,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('user-token')}`,
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }
})

export default main
