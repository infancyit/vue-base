export const LOGIN_ROUTE = '/api/v1/login'
export const REFRESH_TOKEN = '/api/v1/refresh'
export const LOGOUT_ROUTE = '/api/v1/logout'
export const STORE_DETAIL = '/api/v1/me'
