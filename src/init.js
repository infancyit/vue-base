// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import moment from 'moment'
import App from './App'
import router from './router'
import store from './store/index'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-awesome/icons'
import './utils/Interceptor'
import iziToast from './utils/iziToaster'
import Icon from 'vue-awesome/components/Icon'
import { BreedingRhombusSpinner } from 'epic-spinners'

Vue.use(BootstrapVue)
Vue.use(iziToast)

//register global component
Vue.component('icon', Icon)
Vue.component('global-spinner', BreedingRhombusSpinner)

Vue.config.productionTip = false

Vue.prototype.$moment = moment
Vue.prototype.$iziToast = iziToast

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
