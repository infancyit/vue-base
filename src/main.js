// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import router from './router'
import LocalDatabase from './utils/LocalDatabase'
import Types from './store/types'
import store from './store/index'
import './bootstrap'
import './utils/Interceptor'

import LocalForage from 'localforage'

// Configure localforage
LocalForage.config({
  driver: LocalForage.INDEXEDDB,
  name: 'RD',
  version: 1.0,
  storeName: 'rd_management'
})

async function updateStoreForHardReload() {
  const access_token = await LocalDatabase.get(Types.localdb.USER_TOKEN)

  if (access_token) {
    await store.dispatch(Types.actions.UPDATE_USER_TOKEN, {
      access_token
    })

    //await getStoreDetail()
  } else {
    await store.dispatch(Types.actions.UPDATE_AUTH_RESET)
    return {
      redirect: true
    }
  }
}

updateStoreForHardReload()
  .then(function(data) {
    require('./init')
    if (data && data.redirect) {
      router.push({
        name: 'login'
      })
    }
  })
  .catch(error => {
    console.log(error)
  })
