const Login = () => import('../components/Login/Login')
const DashboardLayout = () => import('../layouts/DashboardLayout')

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: {
      guest: true,
      title: 'Login'
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DashboardLayout,
    meta: {
      auth: true,
      title: 'Dashboard'
    }
  }
]

export default routes
