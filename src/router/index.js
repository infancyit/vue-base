import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
import routes from './routes'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes
})

const guestMiddleware = next => {
  if (store.state.auth.token && store.state.auth.token !== 'null') {
    return next({ name: 'dashboard' })
  } else {
    return next()
  }
}

const authMiddleware = next => {
  if (store.state.auth.token && store.state.auth.token !== 'null') {
    return next()
  } else {
    return next({ name: 'login' })
  }
}

const basicMiddleware = next => {
  return next()
}

router.beforeEach((to, from, next) => {
  document.title = to.meta.title

  if (to.meta.basic) {
    basicMiddleware(next)
  }

  if (to.meta.auth) {
    authMiddleware(next)
  }

  if (to.meta.guest) {
    guestMiddleware(next)
  }
})

export default router
