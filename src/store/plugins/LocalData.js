import LocalForage from 'localforage'
import Types from '../types'

// Our store plugin
const persistPlugin = store => {
  store.subscribe((mutation, state) => {
    if (mutation.type === Types.mutations.SET_USER_TOKEN) {
      LocalForage.setItem(Types.localdb.USER_TOKEN, state.auth.token)
      LocalForage.setItem(Types.localdb.EXPIRE_TIME, state.auth.expireTime)
    }
    if (mutation.type === Types.mutations.SET_AUTH_RESET) {
      LocalForage.clear()
    }
  })
}

export default persistPlugin
