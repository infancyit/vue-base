import moment from 'moment'
import Types from '../types'
import LocalDatabase from '../../utils/LocalDatabase'

const auth = {
  state: {
    token: null,
    expireTime: null
  },
  getters: {
    getToken(state) {
      return state.token
    }
  },
  mutations: {
    [Types.mutations.SET_USER_TOKEN](state, payload) {
      state.token = payload.access_token
      state.expireTime = payload.expireTime
    },
    [Types.mutations.SET_AUTH_RESET](state) {
      state.token = null
      state.expireTime = null
    }
  },
  actions: {
    async [Types.actions.UPDATE_USER_TOKEN]({ commit, dispatch }, payload) {
      const isExpireTimeExists = await LocalDatabase.get(
        Types.localdb.EXPIRE_TIME
      )
      const currentTimestamp = moment().unix()
      if (currentTimestamp < isExpireTimeExists) {
        commit(Types.mutations.SET_USER_TOKEN, {
          ...payload,
          expireTime: isExpireTimeExists
        })
      } else if (currentTimestamp > isExpireTimeExists && isExpireTimeExists) {
        await dispatch(Types.actions.UPDATE_AUTH_RESET)
        window.location.href = '/'
      } else {
        const expireTime = moment()
          .add(payload.expires_in, 'm')
          .unix()
        commit(Types.mutations.SET_USER_TOKEN, {
          ...payload,
          expireTime
        })
      }
    },
    async [Types.actions.UPDATE_AUTH_RESET]({ commit }) {
      commit(Types.mutations.SET_AUTH_RESET)
    }
  }
}

export default auth
