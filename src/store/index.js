import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import persistPlugin from './plugins/LocalData'

Vue.use(Vuex)

const store = new Vuex.Store({
  plugins: [persistPlugin],
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    auth
  }
})

export default store
