const Types = {
  getters: {},
  mutations: {
    SET_USER_TOKEN: 'SET_USER_TOKEN',
    SET_AUTH_RESET: 'SET_AUTH_RESET',
    SET_EXPIRE_TIME: 'SET_EXPIRE_TIME'
  },

  actions: {
    UPDATE_USER_TOKEN: 'UPDATE_USER_TOKEN',
    UPDATE_AUTH_RESET: 'UPDATE_AUTH_RESET',
    UPDATE_EXPIRE_TIME: 'UPDATE_EXPIRE_TIME'
  },
  localdb: {
    USER_TOKEN: 'TOKEN',
    EXPIRE_TIME: 'EXPIRE_TIME'
  }
}
export default Types
