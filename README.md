# pwa-fos-fra

> Food Ordering system

## ENV Setup

``` bash

#make a copy of .env.example.js as .env.js

cp .env.example.js .env.js

#After that set the base URL for API server in .env.js

```



## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# Show all linting error
npm run lint

#Fix automatic fixable linting error
npm run lint-autofix

#Build and run on dev server for produnction
serve-prod

```
