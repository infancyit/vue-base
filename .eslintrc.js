// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    allowImportExportEverywhere: true,
    sourceType: 'module',
  },
  globals: {
    "require": true,
    "_":true,
    "google": true,
    "MBirdSdk":true
  },
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: ['eslint:recommended','prettier','prettier/standard','plugin:vue/recommended'],
  // required to lint *.vue files
  plugins: [
    'vue',
    'prettier',
  ],
  // add your custom rules here
  'rules': {
    "prettier/prettier": [
      "error",
      {
        semi: false,
        singleQuote: true
      }
    ],
    "no-console": 0,
    "vue/html-self-closing": "off",
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
